**Rsync** stands for Remote Sync. This tool is used to take backup in Linux and UNIX. But this section is to teach you to exclude specific directory when taking backups. The reference has been taken from [rsync exclude directory and file](http://www.techsakh.com/2016/07/18/rsync-exclude-directory-folder-linux/) tutorial. 

**General syntax**
-------------------

rsync source_path/ destination_path/


**Exclude any directory while taking the backup**
---------------------------------------------

$ rsync -av --exclude 'directory2' source-path/ destination-path/

**Exclude multiple directories**
----------------------------

$ rsync -arv --exclude={directory1,directory2} source-path/ destination-path/